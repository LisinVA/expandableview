
import UIKit

protocol ExpandableViewDelegate: class {
    func didToggleExpansion(view: ExpandableView)
    func needsToAnimateLayout(view: ExpandableView)
}

protocol ExpansionIndicatorable {
    var isExpanded: Bool { get }
    var isAnimatable: Bool { get set }
    var animationDuration: TimeInterval { get set }
    func setExpanded(_ expanded: Bool, animated: Bool)
    
    var titleText: String? { get set }
    var toggleButtonColor: UIColor { get set }
    var contentView: UIView { get }
}

class ExpandableView: UIView {
    
    // MARK: Protocols
    
    weak var delegate: ExpandableViewDelegate?
    
    private(set) var isExpanded: Bool = false
    var isAnimatable: Bool = true
    var animationDuration: TimeInterval = 0.5
    
    var titleText: String? {
        get { return titleLabel.text }
        set { titleLabel.text = newValue }
    }
    var toggleButtonColor: UIColor {
        get { return toggleButton.tintColor }
        set { toggleButton.tintColor = newValue }
    }
    var contentView: UIView { return bodyView }
    
    // MARK: Storyboard
    
    @IBOutlet private weak var titleView: UIView!
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var toggleButton: UIButton!
    @IBOutlet private weak var bodyView: UIView!
    @IBOutlet private var titleBottomToBodyTopConstraint: NSLayoutConstraint!
    @IBAction private func toggleButtonTapped(_ sender: UIButton) {
        setExpanded(!isExpanded, animated: isAnimatable)
    }
    
    // MARK: Lifecycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupView()
    }
    
    private func setupView() {
        clipsToBounds = true
        layer.cornerRadius = 5
        updateUI(animated: false)
    }
}

// MARK: - Updating UI

extension ExpandableView {
    
    private func updateUI(animated: Bool) {
        func performUpdates() {
            updateTitleColor()
            rotateToggleButton()
            updateHeight()
        }
        
        if animated {
            UIView.animate(withDuration: animationDuration) { performUpdates() }
            delegate?.needsToAnimateLayout(view: self)
        } else {
            performUpdates()
        }
    }
    
    private func updateTitleColor() {
        if isExpanded {
            titleView.backgroundColor = .black
        } else {
            titleView.backgroundColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
        }
    }
    
    private func rotateToggleButton() {
        if isExpanded {
            toggleButton.transform = CGAffineTransform(rotationAngle: -1 * .pi)
        } else {
            toggleButton.transform = CGAffineTransform(rotationAngle: 180 * .pi)
        }
    }
    
    private func updateHeight() {
        if isExpanded {
            titleBottomToBodyTopConstraint.isActive = true
        } else {
            titleBottomToBodyTopConstraint.isActive = false
        }
    }
}

// MARK: - ExpansionIndicatorable

extension ExpandableView: ExpansionIndicatorable {
    
    func setExpanded(_ expanded: Bool, animated: Bool) {
        guard expanded != isExpanded else { return }
        isExpanded = !isExpanded
        updateUI(animated: animated)
        delegate?.didToggleExpansion(view: self)
    }
}
