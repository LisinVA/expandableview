
import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var topExpandableView: ExpandableView!
    private var isContentViewConfigured: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupExpandableView()
    }
    
    private func setupExpandableView() {
        topExpandableView.delegate = self
        topExpandableView.isAnimatable = true
        topExpandableView.animationDuration = 0.5
        topExpandableView.titleText = "Grotesque Photo"
        topExpandableView.toggleButtonColor = .red
        topExpandableView.setExpanded(false, animated: false)
        updateContentView()
    }
    
    private func updateContentView() {
        let imageView = UIImageView(image: UIImage(named: "Grotesque-Processor"))
        imageView.contentMode = .scaleAspectFill
        imageView.translatesAutoresizingMaskIntoConstraints = false
        
        let contentView = topExpandableView.contentView
        contentView.addSubview(imageView)
        
        NSLayoutConstraint.activate([
            imageView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            imageView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            imageView.topAnchor.constraint(equalTo: contentView.topAnchor),
            imageView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor),
            imageView.heightAnchor.constraint(equalToConstant: 300)
        ])
    }
}

// MARK: - ExpandableViewDelegate

extension ViewController: ExpandableViewDelegate {
    
    func didToggleExpansion(view: ExpandableView) {
        if view === topExpandableView {
            print("Top View did toggle expansion")
        }
    }
    
    func needsToAnimateLayout(view: ExpandableView) {
        UIView.animate(withDuration: view.animationDuration) {
            self.view.layoutIfNeeded()
        }
    }
}


